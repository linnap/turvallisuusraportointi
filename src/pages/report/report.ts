import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { ReportComponent } from '../../components/report/report';
import { GlobalsProvider } from '../../providers/globals/globals';
import { SpeechProvider } from '../../providers/speech/speech';
import { ImageProvider } from '../../providers/image/image';

/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html'
})
export class ReportPage {
  report;

  @ViewChild(ReportComponent) reportComponent: ReportComponent;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private globals: GlobalsProvider,
    public image: ImageProvider,
    public speech: SpeechProvider,
    public alertCtrl: AlertController) {

    this.report = navParams.get('report');
  }


  ngAfterViewInit() {
    this.reportComponent.switchToViewMode();
    this.reportComponent.report = this.report;
  }

  close() {
    this.viewCtrl.dismiss();
  }

  public saveAndLockReport() {
    var self = this;
    this.report.locked = true;
    this.reportComponent.save().then(function (response) {
      self.reportComponent.switchToViewMode();
      self.globals.presentToast('Raportti tallennetitin ja lukittiin.');
    }).catch(function (error) {
      self.report.locked = false;
      self.globals.presentToast('Raportin lukitseminen ei onnistunut.');
    });
  }

  public deleteReport(report) {
    this.globals.deleteReport(report).then(report => this.close());
  }


}
