import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { SettingsProvider } from '../../providers/settings/settings';
import { EventModel } from '../../models/event-model';

/**
 * Generated class for the LocationModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-location-modal',
	templateUrl: 'location-modal.html',
})
export class LocationModalPage {

	locations;
	selectedLocation;
	type;
	subType;
	send: boolean;


	constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, public settings: SettingsProvider) {
		this.locations = settings.locations;
	}


	filterLocations(ev: any) {
		let context = null;


		if (this.type) {
			context = this.type.children;
		}
		else if (this.selectedLocation) {
			context = this.selectedLocation.children;
		} else {
			context = this.settings.locations;
		}


		let val = ev.target.value;

		if (val && val.trim() !== '') {
			this.locations = context.filter(function (item) {
				return item.name.toLowerCase().includes(val.toLowerCase());
			});
		} else {
			if (context.type != 'location' && context.children)
				return context.children;

			return this.locations = context;
		}

	}

	selectLocation(location) {
		if (location.type == 'location') {
			this.selectedLocation = location;
			this.type = null;
			this.subType = null;
		}
		if (location.type == 'type') {
			this.type = location;
			this.subType = null;
		}
		if (location.type == 'subType') {
			this.subType = location;
		}

		if (location.children) {
			this.locations = location.children;
		} else if (location.send) {
			if (location.send == 'conditional') {
				this.locations = [{
					name: 'Älä lähetä asiakkaalle',
					type: 'sendNo'
				}, {
					name: 'Lähetä asiakkaalle',
					type: 'sendYes'
				}];
			} else {
				this.send = location.send;
			}

		}
		if (location.type == 'sendYes') {
			this.send = true;
		}
		if (location.type == 'sendNo') {
			this.send = false;
		}

		if (this.send !== undefined) {
			this.viewCtrl.dismiss(this.event);
		}

	}


	get event() {
		return {
			send:this.send,
			location:this.selectedLocation.name,
			subType: this.subType && this.subType.name ? this.subType.name : null,
			type: this.type && this.type.name ? this.type.name : null
		};
	}


}
