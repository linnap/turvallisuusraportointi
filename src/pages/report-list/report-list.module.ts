import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportListPage } from './report-list';

@NgModule({
  declarations: [

  ],
  imports: [
    IonicPageModule.forChild(ReportListPage),
  ],
})
export class ReportListPageModule {}
