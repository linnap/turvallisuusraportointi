import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { GlobalsProvider } from '../../providers/globals/globals';
import { ReportPage } from '../../pages/report/report';

//import { ReportComponent } from '../../components/report/report';

/**
 * Generated class for the ReportListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report-list',
  templateUrl: 'report-list.html',
})
export class ReportListPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public globals: GlobalsProvider) {
  }

  public openReport(report) {
    this.navCtrl.push(ReportPage, {report:report});
  }

}
