import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, ActionSheetController } from 'ionic-angular';
import { GlobalsProvider } from '../../providers/globals/globals';

import { ReportModel } from '../../models/report-model';

import { ReportComponent } from '../../components/report/report';
import { SpeechProvider } from '../../providers/speech/speech';
import { ImageProvider } from '../../providers/image/image';

//declare var cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  lastImage: string = null;
  images: Array<string> = [];


  @ViewChild(ReportComponent) reportComponent: ReportComponent;


  report: ReportModel;


  constructor(public navCtrl: NavController,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController,
    public speech: SpeechProvider,
    public image: ImageProvider,
    private globals: GlobalsProvider) {

    if (!this.globals.report.event.location) {
      this.openReportTypeModal();
    }

    this.report = globals.report;
  }


  /**
   * Location and type
   */

  public openReportTypeModal() {
    let modal = this.modalCtrl.create('LocationModalPage', {}, { enableBackdropDismiss: false });

    modal.onDidDismiss(data => {
      if (data && data.location)
        this.report.event = data;
    });

    modal.present();
  }


  /**
   * Report
   */


  public saveReport() {
    var self = this;
    this.globals.saveReport().then(result => {
      self.globals.presentToast('Raportti tallennettiin.');
      self.clearReport();
      self.openReportTypeModal();
    }).catch(error => {
      self.globals.presentToast('Raportin tallentaminen ei onnistunut.');
    });
  }

  public saveAndLockReport() {
    var self = this;
    self.report.locked = true;
    self.globals.saveReport().then(result => {
      self.reportComponent.save().then(function (response) {
        self.report.locked = true;
        self.clearReport();
        self.openReportTypeModal();
        self.globals.presentToast('Raportti tallennetitin ja lukittiin.');
      }).catch(function (error) {
        self.report.locked = false;
        self.globals.presentToast('Raportin lukitseminen ei onnistunut.');
        console.log(error);
      });
    }).catch(error => {
      self.report.locked = false;
      console.log(JSON.stringify(error));
      self.globals.presentToast('Raportin lukitseminen ei onnistunut.');
    });
  }


  public resetReportPage() {
      this.globals.deleteReport(this.report).then(report => {
        this.globals.clearReport();
        this.clearReport();
        this.openReportTypeModal();
      });
  
  }


  clearReport() {
    this.report = this.globals.report;
    this.reportComponent.report = this.report;
  }
}