import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UsersPage } from './users';

@NgModule({
  imports: [
    IonicPageModule.forChild(UsersPage),
  ],
})
export class UsersPageModule {}
