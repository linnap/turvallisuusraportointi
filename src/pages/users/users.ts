import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalsProvider } from '../../providers/globals/globals';

/**
 * Generated class for the UsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-users',
  templateUrl: 'users.html',
})

export class UsersPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public globals: GlobalsProvider) {
  }

  selectUser(user) {
    this.globals.currentUser = user;
    this.navCtrl.pop();
    this.globals.presentToast('Vaihdettiin käyttäjäksi '+user.firstname+' '+user.lastname);
  }

}
