import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

/**
 * Generated class for the ReportModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report-modal',
  templateUrl: 'report-modal.html',
})
export class ReportModalPage {
  report: string;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private viewCtrl: ViewController,
    public modalCtrl: ModalController) {
    this.report = navParams.get('report');
  }

  saveReport() {
    this.viewCtrl.dismiss({ report: this.report });
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
