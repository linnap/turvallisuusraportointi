export interface EventModel {
  location:String;
  type:String;
  subType:string;
  send: boolean;
}