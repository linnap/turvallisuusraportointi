import { EventModel } from './event-model';
import { UserModel } from './user-model';

export interface ReportModel {
  report_time: string,
  user: UserModel,
  images,
  text: string,
  locked: boolean,
  event: EventModel
  audio: string;
}