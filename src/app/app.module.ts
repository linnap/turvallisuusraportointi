import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ReportListPage } from '../pages/report-list/report-list';
import { UsersPage } from '../pages/users/users';
import { ReportPage } from '../pages/report/report';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';

import { Media, MediaObject } from '@ionic-native/media';
import { SpeechRecognition } from '@ionic-native/speech-recognition';

import { GlobalsProvider } from '../providers/globals/globals';
import { ImageProvider } from '../providers/image/image';
import { SettingsProvider } from '../providers/settings/settings';
import { SpeechProvider } from '../providers/speech/speech';

import { ComponentsModule } from '../components/components.module';


import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture';
import { APIProvider } from '../providers/api/api';

import { IonicStorageModule } from '@ionic/storage';

import { AndroidPermissions } from '@ionic-native/android-permissions';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ReportListPage,
    UsersPage,
    ReportPage 
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    ComponentsModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ReportListPage,
    UsersPage,
    ReportPage  
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    Transfer,
    FileTransfer, 
    FileTransferObject,
    AndroidPermissions,

    Media,
    FilePath,
    Camera,
    SpeechRecognition,
    MediaCapture,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalsProvider,
    ImageProvider,
    SettingsProvider,
    SpeechProvider,
    APIProvider
  ]
})


export class AppModule {}
