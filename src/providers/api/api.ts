import { Injectable } from '@angular/core';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class APIProvider {

  constructor() {}

  //Set API key which match with backend database
  get headers() {
    return {
      'X-Authorization': ''
    }
  }

  //Set base url
  get path() {
    return '';
  }

}
