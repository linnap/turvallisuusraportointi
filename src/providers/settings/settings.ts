import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SettingsProvider {

  constructor(public http: HttpClient) {

  }


  get users() {
    return [{
      firstname: 'Erkki',
      lastname: 'Esimerkki'
    }, {
      firstname: 'Timo',
      lastname: 'Testi'
    }];
  }

  get locations() {
    return [{
      name: 'Tehdas',
      type: 'location',
      children: [{
        name: 'Rakenteellinen turvallisuus',
        type: 'type',
        children: [{
          name: 'Avainhallinta',
          send: 'conditional',
          type: 'subType'
        }]
      }, {
        name: 'Avaukset ja sulut',
        send: true,
        type: 'type',
        children: [{
          name: 'Turvallisuusrakenteet',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Kiinteistötekniikka',
          send: 'conditional',
          type: 'subType'
        }]
      }, {
        name: 'Turvallisuusvalvonta',
        send: true,
        type: 'type',
        children: [{
          name: 'Tekninen Turvallisuusvalvonta / silmukkahälytykset (LVIS)',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Tekninen Turvallisuusvalvonta / Muut hälytykset',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Henkilötön, vieraiden ja ajoneuvonen ohjaus',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Vartiointi ja valvomotoiminta',
          send: 'conditional',
          type: 'subType'
        }]
      }, {
        name: 'Vuoron aloitus/päättyminen',
        send: 'conditional',
        type: 'type'
      }, {
        name: 'Paloturvallisuus',
        send: 'conditional',
        type: 'type'
      }]
    }, {
      name: 'Teollisuuslaitos',
      type: 'location',
      children: [{
        name: 'Avaukset ja sulut',
        send: true,
        type: 'type',
        children: [{
          name: 'Turvallisuusrakenteet',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Kiinteistötekniikka',
          send: 'conditional',
          type: 'subType'
        }]
      }, {
        name: 'Työturvallisuus',
        send: 'conditional',
        type: 'type',
        children: [{
          name: 'Työturvallisuushavainnot',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Turvallisuuspoikkeamat',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Läheltä piti-ilmoitukset',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Työtapaturmat',
          send: 'conditional',
          type: 'subType'
        }, {
          name: 'Ympäristöhavainto',
          send: 'conditional',
          type: 'subType'
        }]
      }, {
        name: 'Vuoron aloitus/päättyminen',
        send: 'conditional',
        type: 'type'
      }]
    }]
  }


}
