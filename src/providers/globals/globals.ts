import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController, Platform, AlertController } from 'ionic-angular';
import { ReportModel } from '../../models/report-model';
import { UserModel } from '../../models/user-model';
import { Storage } from '@ionic/storage';
import { SettingsProvider } from '../../providers/settings/settings';
import { ImageProvider } from '../image/image';

import moment from 'moment'
import 'moment/locale/fi';

/*
  Generated class for the GlobalsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalsProvider {

  private reports: Array<Object> = [];
  public report: ReportModel;
  private _currentUser: UserModel;
  private _users: Array<UserModel> = [];

  constructor(
    public http: HttpClient,
    private toastCtrl: ToastController,
    public platform: Platform,
    private settings: SettingsProvider,
    private alertCtrl: AlertController,
    public image: ImageProvider,
    private storage: Storage) {

    this._users = this.settings.users;
    this.clearReport();

    this.storage.ready().then(() => {
      this.storage.get('reports').then(reports => {
        this.reports = reports;
      })

    });
  }

  public presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1000,
      position: 'top'
    });
    toast.present();
  }

  public saveReport() {
      return this.saveToLocalStorage().catch(error => { console.log(error) });
  }

  private saveToLocalStorage() {
    return new Promise((resolve, reject) => {
      this.storage.get('reports').then(reports => {
        var self = this;
        if (!reports) {
          this.storage.set('reports', [this.report]).then(reports => {
            self.reports = reports;
            this.clearReport();
            resolve(true);
          });
        } else {
          reports.unshift(this.report);
          if (reports.length > 10) {
            var r = reports.pop();
            this.removeReportMedia(r).then(() => {
              reports = reports.slice(0, 10);
              this.storage.set('reports', reports).then(() => {
                self.reports = reports;
                this.clearReport();
                resolve(true);
              });
            })
          } else {
            this.storage.set('reports', reports).then(() => {
              self.reports = reports;
              this.clearReport();
              resolve(true);
            });
          }
        }
      }).catch(error => {
        reject(error);
      });

    });
  }

  public reportIsSaved(report) {
    return this.reports.indexOf(report) !== -1;
  }


  public getReports() {
    return this.reports;
  }

  private deleteReportFromStorage(report) {
    return new Promise((resolve, reject) => {
      let index = this.reports.indexOf(report);
      if (index !== -1) {
        this.reports.splice(index, 1);
        this.storage.set('reports', this.reports).then(() => {
          resolve(true);
        });
      }
    });
  }

  private removeReportMedia(report) {
    return new Promise((resolve, reject) => {
      this.image.removeImages(report).then(() => {
          resolve(true);
      }).catch(error => { this.presentToast('Median poistaminen epäonnistui.'); reject(error); });
    });
  }

  public clearReport() {
    this.report = {
      report_time: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
      audio: null,
      user: null,
      images: [],
      text: null,
      locked: null,
      event: {
        location: null,
        type: null,
        subType: null,
        send: false
      }
    };

    this.report.user = this.currentUser;
  }

  isIos() {
    return this.platform.is('ios');
  }

  get currentUser() {
    if (!this._currentUser) {
      this._currentUser = this._users[0];
    }
    return this._currentUser;
  }

  get users() {
    return this._users;
  }

  set currentUser(user: UserModel) {
    this._currentUser = user;
    this.report.user = user;
  }

  getPlatform() {
    return this.platform;
  }


  public deleteReport(report) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: 'Poistetaanko raportti?',
        message: 'Haluatko varmasti poistaa raportin?',
        buttons: [
          {
            text: 'En',
            handler: () => {
            }
          },
          {
            text: 'Kyllä',
            handler: () => {
              this.removeReportMedia(report).then(() => {
                this.deleteReportFromStorage(report);
                this.presentToast('Raportti poistettiin.');
                resolve(report);
              }).catch(error => { this.presentToast('Median poistaminen epäonnistui.'); });
            }
          }
        ]
      });

      alert.present();
    });
  }

}
