import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { SpeechRecognition } from '@ionic-native/speech-recognition';



/*
  Generated class for the SpeechProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


declare var cordova: any;

@Injectable()
export class SpeechProvider {

  public isRecording = false;


  constructor(
    private platform: Platform,
    private zone: NgZone,
    public events: Events,
    private speechRecognition: SpeechRecognition
  ) {
    this.events.subscribe('updateScreen', () => {
      this.zone.run(() => {
        console.log('force update the screen');
      });
    });
  }




  private doRecognize(report) {
    // Start the recognition process
    //for ios
    this.isRecording = true;
    this.speechRecognition.startListening({
      language: 'fi-FI',
      matches: 1
    }).subscribe(
      (matches) => {
        report.text = matches[0];
        //ios won't update view without this
        this.events.publish('updateScreen');
      },
      (onerror) => console.log('error:', onerror)
    )
  }

  //for ios
  public stopListening() {
    this.isRecording = false;
    this.speechRecognition.stopListening();
  }


  public recognizeSpeech(report) {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (hasPermission) {
          this.doRecognize(report);
        } else {
          console.log('Request permission');
          this.speechRecognition.requestPermission()
            .then(
              () => {
                this.doRecognize(report);
              },
              () => console.log('Permission denied.')
            )
        }
      })


  }

}
