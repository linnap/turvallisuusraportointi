import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Camera } from '@ionic-native/camera';
import { ActionSheetController, Platform, normalizeURL, ToastController, } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { APIProvider } from '../../providers/api/api';
import { ReportModel } from '../../models/report-model';
import { DomSanitizer } from '@angular/platform-browser';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';


/*
  Generated class for the ImageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

declare var cordova: any;

@Injectable()
export class ImageProvider {

  public report: ReportModel;
  public test;

  constructor(
    public http: HttpClient,
    private camera: Camera,
    private file: File,
    private fileTransfer: FileTransfer,
    private filePath: FilePath,
    private toastCtrl: ToastController,
    public domSanitizer: DomSanitizer,
    public actionSheetCtrl: ActionSheetController,
    private api: APIProvider,
    public platform: Platform) {
  }


  public presentActionSheet(report) {

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Valitse lähde',
      buttons: [
        {
          text: 'Lataa kirjastosta',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, report);
          }
        },
        {
          text: 'Käytä kameraa',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA, report);
          }
        },
        {
          text: 'Peruuta',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  private getPlatform() {
    return this.platform;
  }


  public takePicture(sourceType, report) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };


    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.getPlatform().is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName(), report);
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName(), report);
      }
    }, (err) => {
      this.presentToast('Kuvan valinnassa tapahtui virhe.');
    });
  }

  public presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1000,
      position: 'top'
    });
    toast.present();
  }


  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName, report) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      report.images.push(newFileName);
    }, error => {
      this.presentToast('Kuvan tallennuksessa tapahtui virhe.');
    });
  }


  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public normalPathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return normalizeURL(cordova.file.dataDirectory + img);
    }
  }

  public removeImage(report, file): Promise<any> {
    //console.log(file)
    return this.file.removeFile(cordova.file.dataDirectory, file).then((data) => {
      if (data.success) {
        var index = report.images.indexOf(file, 0);
        if (index > -1) {
          report.images.splice(index, 1);
        }
        return true;
      } else {
        this.presentToast('Kuvaa ei voitu poistaa!');
        return false;
      }
    }).catch(error => {
      console.log(JSON.stringify(error))
      return error;
    })
  }


  public removeImages(report) {
    //jos ei kuvia palataan heti
    if (report.images.length < 1) {
      return new Promise((resolve, reject) => {
        if (1 == 1)
          resolve([]);
        else
          reject('');
      });
    }

    var images = report.images.slice();

    for (let i of images) {
      return this.removeImage(report, i);
    }
  }

  public uploadImages(report) {
    //jos ei kuvia palataan heti
    if (report.images.length < 1) {
      return new Promise((resolve, reject) => {
        if (1 == 1)
          resolve([]);
        else
          reject('');
      });
    }

    var images = report.images.slice();
    var uploadedImages: Array<String> = [];

    return new Promise((resolve, reject) => {
      for (let index = 0; index < images.length; index++) {
        this.uploadImage(images[index]).then((response) => {
          uploadedImages.push(response.response);
          if (uploadedImages.length == images.length) {
            resolve(uploadedImages);
          }
        }).catch((err) => {
          reject({ error: 'Kuvan lisääminen epäonnistui' });
        });
      }
    });

  }

  private uploadImage(image) {
    if (!this.api.path) {
      console.log('API base url is empty.');
      return;
    }
    // Destination URL
    var url = this.api.path + "report/save/images";

    // File for Upload
    var targetPath = this.pathForImage(image);

    // File name only
    var filename = image;

    var options = {
      fileKey: "image",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: { 'fileName': filename },
      headers: this.api.headers
    };

    const fileTransfer: FileTransferObject = this.fileTransfer.create();

    return fileTransfer.upload(targetPath, url, options);

  }

}
