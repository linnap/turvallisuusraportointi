import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ReportComponent } from './report/report';

@NgModule({
	declarations: [ReportComponent],
	imports: [IonicModule],
	exports: [ReportComponent]
})
export class ComponentsModule {}
