import { Component } from '@angular/core';
import { ModalController, ActionSheetController, LoadingController, Loading } from 'ionic-angular';

import { GlobalsProvider } from '../../providers/globals/globals';
import { APIProvider } from '../../providers/api/api';
import { SpeechProvider } from '../../providers/speech/speech';
import { ImageProvider } from '../../providers/image/image';

import { ReportModel } from '../../models/report-model';

import moment from 'moment'
import 'moment/locale/fi';

import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the ReportComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

declare var cordova: any;

@Component({
  selector: 'report',
  templateUrl: 'report.html'
})
export class ReportComponent {

  public report: ReportModel;
  private _viewMode: string = 'edit';
  private loader: Loading;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private globals: GlobalsProvider,
    private api: APIProvider,
    public speech: SpeechProvider,
    public image: ImageProvider,
    private http: HttpClient) {

    this.report = globals.report;
  }


  /** text */

  public openReportModal() {
    console.log(this.report);
    let modal = this.modalCtrl.create('ReportModalPage', { report: this.report.text });

    modal.onDidDismiss(data => {
      if (data)
        this.report.text = data.report;
    });

    modal.present();
  }

  /** speech */

  toggleViewMode() {
    this._viewMode = this._viewMode == 'edit' ? 'view' : 'edit';
  }

  switchToEditMode() {
    this._viewMode = 'edit';
  }

  switchToViewMode() {
    this._viewMode = 'view';
  }

  get viewMode() {
    return this._viewMode;
  }

  formatDate() {
    return moment(this.report.report_time).format('DD.MM.YYYY [klo] HH.mm');
  }

  get dateTime() {
    return moment(this.report.report_time).format();
  }

  set dateTime(dateTime) {
    this.report.report_time = moment(dateTime).format('YYYY-MM-DD HH:mm:ss');
  }

  save() {
    if (!this.api.path) {
      console.log('API base url is empty.');
      return;
    }
    this.showLoader();
    var report = Object.assign({}, this.report);
    return this.image.uploadImages(report).then((uploadedImages) => {
      report.images = uploadedImages;
        return this.http.post(this.api.path + 'report/save', report, { headers: this.api.headers }).toPromise()
          .then((response) => { this.hideLoader(); return response; })
          .catch((response) => { this.hideLoader(); throw response; });
    }).catch((response) => { this.hideLoader(); throw response; });
  }


  showLoader(message = 'Tallennetaan') {
    this.loader = this.loadingCtrl.create({
      spinner: 'crescent',
      content: message
    });

    this.loader.present();
  }

  hideLoader() {
    this.loader.dismiss();
  }


}
